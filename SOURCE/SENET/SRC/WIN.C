/*
    win.c
    Functions to display windows and dialog boxes.
    copyright (C) 2020 Jim Hall <jhall@freedos.org>

    This is free software under the GNU GPL (only version 2).
    See the file COPYING for the full text of the GNU GPL.
*/

#include <graph.h>
#include <i86.h> /* sound(), nosound(), delay() */
#include <string.h> /* strlen() */

/*
    reminder on colors:
    iRGB # color:   iRGB # color:
    0000 0 black    1000 8 br black
    0001 1 blue     1001 9 br blue
    0010 2 green    1010 10 br green
    0011 3 cyan     1011 11 br cyan
    0100 4 red      1100 12 br red
    0101 5 magenta  1101 13 br magenta
    0110 6 brown    1110 14 yellow
    0111 7 white    1111 15 br white
*/

void
popup_message(char *msg)
{
    int col, width;

    width = strlen(msg) + 2; /* add extra space around msg */

    col = (80 - width) / 2; /* center the popup window */

    /* draw the drop shadow */

    _settextwindow(11,col+1, 14,col+width+1);
    _setbkcolor(0); /* black */
    _clearscreen(_GWINDOW);

    /* draw the popup window */

    _settextwindow(10,col, 13,col+width);
    _setbkcolor(4); /* red */
    _clearscreen(_GWINDOW);

    /* print message */

    _settextcolor(15); /* br white */
    _settextposition(2,2);
    _outtext(msg);
}

void
print_message(char *msg)
{
    /* print message at the bottom of the screen */

    _settextwindow(25,1, 25,80);
    _setbkcolor(6); /* brown */
    _clearscreen(_GWINDOW);

    _settextcolor(15); /* br white */
    _settextposition(1,1);
    _outtext(msg);
}

void
print_error(char *msg)
{
    /* print an error message at the bottom of the screen */

    _settextwindow(25,1, 25,80);
    _setbkcolor(4); /* red */
    _clearscreen(_GWINDOW);

    _settextcolor(14); /* yellow */
    _settextposition(1,1);
    _outtext(msg);

    /* make a short beep */

    sound(987); /* C b */
    delay(50); /* milliseconds */
    nosound();

    /* wait a moment, then reset the message line to normal */

    delay(1000); /* milliseconds */
    print_message("");

    /* make another short beep */

    sound(784); /* C */
    delay(50); /* milliseconds */
    nosound();
}

void
print_title(char *title)
{
    /* print a title at the top of the screen */

    int col;

    _settextwindow(1,1, 1,80);
    _setbkcolor(6); /* brown */
    _clearscreen(_GWINDOW);

    /* center the text */

    col = (80 - strlen(title)) / 2;

    if (col < 1) {
	col = 1;
    }

    _settextcolor(15); /* br white */
    _settextposition(1,col);
    _outtext(title);
}
